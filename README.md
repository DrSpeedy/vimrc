# vimrc
My vimrc settings

**Install**
```
$ git clone https://github.com/DrSpeedy/vimrc.git ~/.vim
$ mkdir ~/.vim/bundle ~/.vim/colors
$ git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
$ wget -O ~/.vim/colors/atom-dark.vim https://raw.githubusercontent.com/gosukiwi/vim-atom-dark/master/colors/atom-dark.vim
$ ln -s ~/.vim/vimrc ~/.vimrc
```
Open gVim and enter the following command `:PluginInstall` to finish off the installation
