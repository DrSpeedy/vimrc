set nocompatible					"Latest vim settings/options

so ~/.vim/plugins.vim				"vundle plugin data

syntax enable

set backspace=indent,eol,start		"Make backspace behave like every other editor
let mapleader=','					"set <Leader>

"------------Visuals------------"
colorscheme atom-dark				"set the theme for vim
set t_CO=256						"force 256 colors if using terminal vim
set guifont=Liberation\ Mono\ 10	"set font to Liberation-Mono with fontsize 10
set linespace=9						"gVim-specific line-height

"Disable scrollbars in gVim
set guioptions-=l
set guioptions-=L
set guioptions-=r
set guioptions-=R

set guioptions-=m	"disable menubar
set guioptions-=T	"disable toolbar

"------------Searching------------"
set hlsearch		"highlight search results
set incsearch		"highlight search results as they're typed

"------------Split Management------------"
set splitbelow		"Make horizontal splits open below by default
set splitright		"Make vertical splits open to the right by defualt

"Simpler mapping to switch between splits (ex. Ctrl-J = Ctrl-W-J)
nmap <C-J> <C-W><C-J>
nmap <C-K> <C-W><C-K>
nmap <C-H> <C-W><C-H>
nmap <C-L> <C-W><C-L>

"------------Editor------------"
set number			"Enable line numbers
set tabstop=4		"size of hard tabstop		
set shiftwidth=4	"size of indent
set softtabstop=4	"use both spaces and tabs to simulate tabstops at a width
set smarttab		"indent new lines
set expandtab		"replace spaces with space

"------------Mappings------------"
"Save files with sudo (root)
cmap w!! w !sudo tee > /dev/null %

"Make it easy to edit Vimrc file.
nmap <Leader>ev :e $MYVIMRC<cr>

"clear search results	
nmap <Leader><space> :nohlsearch<cr>		

"toggle NERD Tree
nmap <Leader>n :NERDTreeToggle<cr>

"open CtrlP tag search (Ctrl-R)
nmap <C-R> :CtrlPBufTag<cr>

"open CtrlP recent files (Ctrl-E)
nmap <C-E> :CtrlPRMUFiles<cr>

"------------Auto Commands------------"
augroup autosourcing
	autocmd!
	autocmd BufWritePost vimrc source %	"Automatically source the Vimrc file on save
augroup END
